## Suma tus comunidades

Es requisito que para agregar una comunidad a nuestras listas quien realice la petición sea el creador o un miembro del staff con la aprobación de todo el staff. Asi mismo la comunidad debe participar activamente y/o contribuir con el proyecto. Recuerden que para Jugar o usar los servidores no es necesario unirse.


## ¿Cómo realizo la petición?

De momento la forma de realizar la petición es con un Issue en GitLab (En este mimos repositorio agregar un Issue) En el mismo se avisara si la comunidad es aceptada o no y cuando ya figure oficialmente como parte de la comunidad.

## ¿Que debe contener el Issue?

Para que podamos aceptarlo necesitamos en el issue la siguiente documentación

* Nombre de la Comunidad
* Info (Algún pequeño texto)
* Imagen/Logo
* Web (Si la tiene)
* Telegram (Si lo tiene)
* Función a cumplir (La comunidad colaborará activamente en LLG, aquí pretendemos ponernos de acuerdo con que colabora cada comunidad)

De la misma forma se espera tener una comunicación abierta con el Staff de la comunidad en el caso que sea necesario.

## Mucho muy importante

Cada comunidad que ingresa debe tener un Representante (puede ser hasta 3 por comunidad) que participe activamente en las votaciones y decisiones de LLG, tendrá el mismo derecho a Voz y Voto que todos los miembros.