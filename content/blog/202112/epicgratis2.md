---
title: "Obtener Shenmue 3 FREE"
img: "https://cdn1.epicgames.com/5d582c08e31a43128a61093a2c3ff7f0/offer/Diesel_productv2_shenmue-3_home_S3_Editions_StandardEdition-(1)-3840x2160-b52887d728ec986309c80e7c3bef5962a702abd7-3840x2160-4be4e333fa890045c6a24f93008b3291.jpg"
date: "2021-12-16 13:00:00"
tags: ["epic", "free", "epicgames", "gratis", "shenmue 3", "shenmue"]
---

> Juega como Ryo Hazuki, un experto en artes marciales japonés de 18 años ávido de venganza por la muerte de su padre.

Juega como Ryo Hazuki, un experto en artes marciales japonés de 18 años ávido de venganza por la muerte de su padre.

En esta tercera entrega de la épica saga Shenmue, Ryo intentará resolver el misterio que se oculta tras el Espejo del Fénix, un artefacto codiciado por el asesino de su padre. Su viaje lo llevará a una inmersiva representación de la China rural, rebosante de actividad y bellos paisajes.

{{< img src="https://i.ytimg.com/vi/Jbv6YzWjeQQ/maxresdefault.jpg" >}}

La aventura de Ryo lo llevará a ciudades y aldeas de montaña donde perfeccionará sus habilidades, pondrá a prueba su suerte en los juegos de azar y las máquinas recreativas y trabajará a tiempo parcial mientras investiga a aquellos que conocen la verdad detrás del Espejo del Fénix.

### Requisitos

* Intel Core i5-4460 a 3,40 GHz o superior; Quad-Core o superior
* 4 GB de RAM
* NVIDIA GeForce GTX 650 Ti o superior (requiere una tarjeta con DirectX 11 y 2 GB de VRAM)
* 100 GB de espacio disponible
* Conexión a Internet de banda ancha

{{< link url="https://www.epicgames.com/store/es-ES/p/shenmue-3" text="Obtener Gratis" >}}
