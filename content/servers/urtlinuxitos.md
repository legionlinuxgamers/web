---
title: "Urban Terror [LiNuXiToS]"
date: "2020-06-04 08:10:00"
tags: ["Urban Terror", "urt", "urban", "servidor", "terror", "server urt"]
category: ["Servers"]
img: "/img/games/urbanterrorbanner.webp"
logo: "/img/games/urbanterrorlogo.png"
ip: "linuxitos.ddnsking.com"
port: 27960
code: "urt"
nointro: false
onlinetime: "OnDemand"
resumen: "Urban Terror es un juego de disparos en primera persona y multijugador gratuito. Urban Terror utiliza el motor ioquake3, además de ser un complemento del Quake III Arena. Este juego se centra en combinar el realismo con la acción acelerada de los tiradores, como Quake III Arena y Unreal Tournament."
requisitos: ["Linux Ubuntu 12.04", "Pentium 4 1.2GHz o superior", "256MBs RAM", "Tarjeta NVidia o ATI con 128 MB de RAM", "Conexión de banda ancha a Internet", "50GB Disco Duro"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
