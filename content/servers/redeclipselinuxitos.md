---
title: "Red Eclipse 2 [LiNuXiToS]"
date: "2020-07-07 08:10:00"
tags: ["red eclipse 2", "shooter", "servidor", "redeclipse2", "server red eclipse 2"]
category: ["Servers"]
img: "/img/games/re2banner.webp"
logo: "/img/games/re2logo.png"
ip: "linuxitos.ddnsking.com"
port: 28801
code: "quake1"
onlinetime: "24/7"
nointro: false
resumen: "Sobre la base de más de 12 años de diseño de juegos, Red Eclipse 2 se centra en un juego de disparos en primera persona de ritmo rápido, ágil y de fácil acceso. Para los creadores, todo el poder del motor Tesseract está al alcance de su mano en el editor multijugador en tiempo real incluido."
requisitos: ["SteamOS 1.0 / Ubuntu 14.04", "Intel Pentium Dual-Core E2180 / AMD Athlon X2 4050", "2 GB de RAM", "Intel HD 630 / Nvidia GeForce GT 630 / AMD Radeon HD 5750", "Conexión de banda ancha a Internet", "2 GB de espacio disponible", "OpenGL 2.0 with GLSL 1.20"]
---

{{< server/servernodata >}}
