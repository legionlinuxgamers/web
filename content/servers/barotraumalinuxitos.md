---
title: "Barotrauma [LiNuXiToS]"
date: "2022-06-07 08:10:00"
tags: ["Barotrauma", "shooter", "servidor", "barotrauma", "sandbox", "rpg"]
category: ["Servers"]
img: "/img/games/barotraumabanner.webp"
logo: "/img/games/barotraumalogo.png"
ip: "linuxitos.ddnsking.com"
port: 28770
code: "barotrauma"
onlinetime: "24/7"
nointro: false
resumen: "Barotrauma es un simulador de submarino cooperativo en 2D con elementos de survival horror. Pilota tu submarino, da órdenes, lucha contra monstruos, arregla fugas, usa máquinas, crea objetos y mantente alerta: ¡en Barotrauma, el peligro llega sin avisar!"
requisitos: ["Ubuntu 18.04 LTS", "Dual Core 2.4 GHz", "4 GB de RAM", "Conexión de banda ancha a Internet", "1 GB de espacio disponible", "2 GB memory (dedicated VRAM or shared RAM), OpenGL 3.0+ support"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}

</br></br>

##### Contraseña

</br>

Para poder ingresar en el servidor pedirá la contraseña, la misma puedes obtenerla en nuestro canal de telegram.
