---
title: "Insurgency [PatoJAD]"
date: "2022-05-09 08:10:00"
tags: ["insurgency", "accion", "shooter", "servidor", "multiplayer", "server insurgency"]
category: ["Servers"]
img: "/img/games/insurgencybanner.webp"
logo: "/img/games/insurgencylogo.png"
ip: "patojad.mooo.com"
port: 27031
code: "insurgency"
onlinetime: "24/7"
nointro: false
resumen: "Sumérgete en una experiencia de combate en espacios cerrados en la que el éxito de tu equipo dependerá de su capacidad para asaltar puntos estratégicos o destruir arsenales, entre otros objetivos. Toma las calles en este título multijugador y cooperativo basado en el motor Source."
requisitos: ["Ubuntu 12.04", "Dual core from Intel® or AMD at 2.8 GHz", "4 GB RAM", "NVIDIA GeForce 8600/9600GT, ATI/AMD Radeon HD2600/3600 (Graphic Drivers: nVidia 310, AMD 12.11), OpenGL 2.1", "Conexión de banda ancha a Internet", "10 GB available space", "OpenAL Compatible Sound Card"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
