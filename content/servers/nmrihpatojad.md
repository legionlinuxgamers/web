---
title: "No More Room in Hell [PatoJAD]"
date: "2020-06-05 08:10:00"
tags: ["No More Room in Hell", "nmrih", "nmrh", "servidor", "zombies", "server nmrih"]
category: ["Servers"]
img: "/img/games/nmrihbanner.webp"
logo: "/img/games/nmrihlogo.png"
ip: "patojad.mooo.com"
port: 27017
code: "nmrih"
onlinetime: "24/7"
nointro: false
resumen: "Las posibilidades de que sobrevivas a esta guerra total de la sociedad y los no muertos son escasas o nulas. Ya hay millones de muertos vivientes deambulando, buscando comida para comer. No hay cura conocida. Un bocado puede acabar con todo por ti. Sin embargo, no estás solo en esta pesadilla."
requisitos: ["Ubuntu 14.04LTS or better", "Dual core 2.8 GHz processor or better", "2 GB RAM", "Radeon HD 5000 Series or nVidia GeForce 200 series or better", "Conexión de banda ancha a Internet", "10 GB HD space"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
