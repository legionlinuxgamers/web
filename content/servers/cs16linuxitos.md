---
title: "Counter-Strike 1.6 [LiNuXiToS]"
date: "2020-06-01 08:10:00"
tags: ["counter strike", "cs", "cs 1.6", "servidor", "counter 1.6", "server counter"]
category: ["Servers"]
img: "/img/games/cs16banner.webp"
logo: "/img/games/cs16logo.png"
ip: "linuxitos.ddnsking.com"
port: 27016
code: "cs16"
onlinetime: "OnDemand"
nointro: false
resumen: "Disfruta del juego de acción en línea n° 1 en el mundo. Sumérgete en el fragor de la guerra antiterrorista más realista con este archiconocido juego por equipos. Alíate con compañeros para superar misiones estratégicas, asalta bases enemigas, rescata rehenes, y recuerda que tu personaje contribuye al éxito del equipo y viceversa."
requisitos: ["Linux Ubuntu 12.04", "Dual-core from Intel or AMD at 2.8 GHz", "1GB RAM", "nVidia GeForce 8600/9600GT, ATI/AMD Radeaon HD2600/3600 (Graphic Drivers: nVidia 310, AMD 12.11)", "OpenGL 2.1", "4GB Disco Duro", "Placa de sonido compatible con OpenAL"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
