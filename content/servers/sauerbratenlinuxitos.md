---
title: "Sauerbraten [LiNuXiToS]"
date: "2020-07-02 08:10:00"
tags: ["Sauerbraten", "sauer", "shooter", "servidor", "sauerbraten", "server sauerbraten"]
category: ["Servers"]
img: "/img/games/sauerbratenbanner.webp"
logo: "/img/games/sauerbratenlogo.png"
ip: "linuxitos.ddnsking.com"
port: 28785
code: "wolfensteinet"
onlinetime: "OnDemand"
nointro: false
resumen: "Sauerbraten es, al mismo tiempo, un videojuego y un motor de juego para videojuegos de acción en primera persona. Está publicado como software libre. En el videojuego se incluyen escenarios, personajes, armas, texturas y modelos tridimensionales."
requisitos: ["CPU de 1 GHz o superior", "256MB RAM", "Tarjeta de video GeForce 4 MX o equivalente.", "Linux Ubuntu 12.04", "Placa de sonido compatible con OpenAL"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
