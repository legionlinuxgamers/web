---
title: "Smokin' Gun [LiNuXiToS]"
date: "2020-07-09 08:10:00"
tags: ["smokin gun", "shooter", "servidor", "fps", "server smokin"]
category: ["Servers"]
img: "/img/games/smokingunsbanner.webp"
logo: "/img/games/smokingunslogo.png"
ip: "linuxitos.ddnsking.com"
port: 27962
code: "quake3"
onlinetime: "24/7"
nointro: false
resumen: "Smokin' Guns es un videojuego del género First Person Shooter.El juego pretende ser una simulación realista de la atmósfera del Viejo Oeste americano. El juego, así como las ubicaciones se inspiran en las películas occidentales de werstern,1​en particular las del género Spaghetti Western."
requisitos: ["Pentium 2 3000MHZ Processor", "Conexión de banda ancha a Internet", "64 Mb de RAM", "400mb de espacio disponible"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
