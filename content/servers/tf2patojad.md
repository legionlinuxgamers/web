---
title: "Team Fortress 2 [PatoJAD]"
date: "2021-02-17 08:10:00"
tags: ["teamfortress", "tf2", "team", "servidor", "fortress", "tf"]
category: ["Servers"]
img: "/img/games/tf2banner.webp"
logo: "/img/games/tf2logo.png"
ip: "patojad.mooo.com"
port: 27016
code: "tf2"
onlinetime: "24/7"
nointro: false
resumen: "Nueve clases diferentes ofrecen una amplia variedad de habilidades tácticas y personalidades. Constantemente actualizado con nuevos modos de juego, mapas, equipamiento y, lo que es más importante, ¡sombreros!"
requisitos: ["Ubuntu 12.04", "Dual core from Intel or AMD at 2.8 GHz", "1 GB de RAM", "nVidia GeForce 8600/9600GT, ATI/AMD Radeon HD2600/3600 (Graphic Drivers: nVidia 310, AMD 12.11), OpenGL 2.1", "Conexión de banda ancha a Internet", "15 GB de espacio disponible", "OpenAL Compatible Sound Card"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
