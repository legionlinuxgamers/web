---
title: "Xonotic [LiNuXiToS]"
date: "2020-07-04 08:10:00"
tags: ["xonotic", "shooter", "servidor", "quake", "server xonotic"]
category: ["Servers"]
img: "/img/games/xonoticbanner.webp"
logo: "/img/games/xonoticlogo.png"
ip: "linuxitos.ddnsking.com"
port: 26000
code: "quake3"
onlinetime: "24/7"
nointro: false
resumen: "Warsow es un videojuego del género disparos en primera persona. La primera versión fue liberada el 8 de junio de 2005 como una versión alfa. El juego esta en constante desarrollo. La primera versión estable salió el 28 de julio de 2012."
requisitos: ["Requiere un procesador y un sistema operativo de 64 bits", "Intel Core i3 2100 / AMD Phenom II x4 955", "Conexión de banda ancha a Internet", "1 GB VRAM - NVIDIA GeForce GTX 470 / ATI Radeon HD5800", "Placa de sonido compatible con OpenAL", "4 GB de RAM", "3 GB de espacio disponible"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
