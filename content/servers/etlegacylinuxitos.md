---
title: "ET Legacy [LiNuXiToS]"
date: "2020-06-02 08:10:00"
tags: ["et legacy", "et", "etlegacy", "servidor", "wolfenstein", "server et"]
category: ["Servers"]
img: "/img/games/etbanner.webp"
logo: "/img/games/etlogo.png"
ip: "linuxitos.ddnsking.com"
port: 27960
code: "wolfensteinet"
onlinetime: "OnDemand"
nointro: false
resumen: "ET: Legacy, un proyecto de código abierto que tiene como objetivo crear un cliente y un servidor totalmente compatibles para el popular juego de FPS en línea Wolfenstein: Enemy Territory, cuya jugabilidad todavía es considerada incomparable por muchos, a pesar de su gran antigüedad."
requisitos: ["Pentium III 600 MHz processor or AMD equivalent.", "128 MB RAM", "3D hardware accelerator with 32 MB with full OpenGL support", "500 MB de espacio disponible", "Placa de sonido compatible con OpenAL"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
