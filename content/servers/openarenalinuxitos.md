---
title: "OpenArena [LiNuXiToS]"
date: "2020-06-08 08:10:00"
tags: ["openarena", "open", "arena", "servidor", "shooter", "server oa"]
category: ["Servers"]
img: "/img/games/openarenabanner.webp"
logo: "/img/games/openarenalogo.png"
ip: "linuxitos.ddnsking.com"
port: 27960
code: "quake3"
onlinetime: "OnDemand"
nointro: false
resumen: "OpenArena es un videojuego en 3D Libre, perteneciente al género de acción en primera persona. OpenArena fue lanzado al mercado como la primera prueba de beta el 19 de agosto de 2005, un día después de que el código fuente del motor gráfico de Quake III fuera liberado bajo Licencia GPL."
requisitos: ["Ubuntu 12.04 or newer", "Pentium II 233 MHz o AMD 350 MHz K6-2 o Athlon", "64 MB RAM", "Tarjeta de vídeo: 8 MB", "427 MB de espacio disponible", "Placa de sonido compatible con OpenAL"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
