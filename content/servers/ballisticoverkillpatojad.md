---
title: "Ballistic Overkill [PatoJAD]"
date: "2020-08-27 08:10:00"
tags: ["Ballistic Overkill", "shooter", "servidor", "ballistic overkill", "ballistic", "overkill"]
category: ["Servers"]
img: "/img/games/bobanner.webp"
logo: "/img/games/bologo.png"
ip: "patojad.mooo.com"
port: 28770
code: "quake1"
onlinetime: "24/7"
nointro: false
resumen: "Ballistic: Overkill es un shooter PvP de ritmo frenético. Fácil de aprender a manejar pero dificil de dominar. Sin complicadas configuraciones, las reglas son simples: ¡matar o ser aniquilado!"
requisitos: ["Linux Ubuntu 12.04 or newer", "Intel Core 2 Duo 2.0 GHZ or better", "2 GB RAM", "Conexión de banda ancha a Internet", "600 MB de espacio disponible", "OpenGL 2.0 with GLSL 1.20"]
---

{{< server/servernodata >}}
